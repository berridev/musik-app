import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttery_seekbar/fluttery_seekbar.dart';
import 'dart:math';

void main() => runApp(MaterialApp(
      home: Home(),
      title: "Musik App",
      debugShowCheckedModeBanner: false,
    ));

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

var warna1 = 0xFF424043;
var warna2 = 0xFFafafaf;
var warbg = 0xFF312D32;
var skunder = 0xFFb440f7;

class _HomeState extends State<Home> {
  double _thumbPercent = 0.4;

  Widget _buildRadialSeekBar() {
    return RadialSeekBar(
      trackColor: Colors.white.withOpacity(.5),
      trackWidth: 2.0,
      progressColor: Color(warna1),
      progressWidth: 5.0,
      thumbPercent: _thumbPercent,
      thumb: CircleThumb(
        color: Color(warna1),
        diameter: 20.0,
      ),
      progress: _thumbPercent,
      onDragUpdate: (double percent) {
        setState(() {
          _thumbPercent = percent;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Color(warbg),
      appBar: AppBar(
        backgroundColor: Color(warna1),
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () {},
        ),
        title: Text("Biasa aja cuy",
            style: TextStyle(color: Colors.white, fontFamily: "Nexa")),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.menu, color: Colors.white),
            onPressed: () {},
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20.0,
            ),
            Center(
              child: Container(
                width: 250.0,
                height: 250.0,
                child: Stack(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          color:
                              Color(skunder).withOpacity(.5), // lingkaran foto
                          shape: BoxShape.circle),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: _buildRadialSeekBar(),
                      ),
                    ),
                    Center(
                      child: Container(
                        width: 200.0,
                        height: 200.0,
                        child: Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: ClipOval(
                            clipper: MClipper(),
                            child: Image.asset("assets/images/ed-sheeran.jpg",
                                fit: BoxFit.cover),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 25.0,
            ),
            Column(
              children: <Widget>[
                Text(
                  "Shape Of You - Ed Sheeran",
                  style: TextStyle(
                    color: Color(warna2),
                    fontSize: 20.0,
                    fontFamily: "Nexa",
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  "Devide",
                  style: TextStyle(
                      color: Color(warna2),
                      fontSize: 18.0,
                      fontFamily: "NexaLight"),
                ),
              ],
            ),
            SizedBox(
              height: 5.0,
            ),
            Container(
              width: 350,
              height: 150,
              child: Stack(
                children: <Widget>[
                  Center(
                    child: Container(
                      height: 65.0,
                      width: 290.0,
                      decoration: BoxDecoration(
                          border: Border.all(color: Color(warna2), width: 3.0),
                          borderRadius: BorderRadius.circular(40.0)),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25.0),
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.fast_rewind,
                                size: 55.0, color: Color(warna2)),
                            Expanded(
                              child: Container(),
                            ),
                            Icon(Icons.fast_forward,
                                size: 55.0, color: Color(warna2)),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      width: 92.0,
                      height: 92.0,
                      decoration: BoxDecoration(
                          color: Color(skunder), shape: BoxShape.circle),
                      child: IconButton(
                        icon: Icon(
                          Icons.play_arrow,
                          size: 45.0,
                          color: Colors.white,
                        ),
                        onPressed: () {},
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 190.0,
              width: double.infinity,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    left: -25.0,
                    child: Container(
                      width: 50.0,
                      height: 190.0,
                      decoration: BoxDecoration(
                        color: Color(skunder),
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30.0),
                          bottomRight: Radius.circular(30.0),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    right: -25,
                    child: Container(
                      width: 50.0,
                      height: 190.0,
                      decoration: BoxDecoration(
                        color: Color(skunder),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          bottomLeft: Radius.circular(30),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        lagu("assets/images/maroon5.jpg", "Maroon 5", "Sugar"),
                        lagu("assets/images/dualipa.png", "Dua Lipa",
                            "New Rules"),
                        lagu("assets/images/khalid.png", "Khalid", "Love Lies"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget lagu(String gambar, String penyanyi, String lagu) {
  return Padding(
    padding: EdgeInsets.all(8.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image.asset(
          gambar,
          width: 40.0,
          height: 40.0,
        ),
        SizedBox(
          width: 8.0,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(penyanyi, style: TextStyle(color: Color(skunder))),
            SizedBox(
              height: 6.0,
            ),
            Text(lagu, style: TextStyle(color: Color(warna2))),
          ],
        )
      ],
    ),
  );
}

class MClipper extends CustomClipper<Rect> {
  @override
  Rect getClip(Size size) {
    return Rect.fromCircle(
        center: Offset(size.width / 2, size.height / 2),
        radius: min(size.width, size.height) / 2);
  }

  @override
  bool shouldReclip(CustomClipper<Rect> oldClipper) {
    return true;
  }
}
